var images = document.querySelectorAll(".image");
var container = document.querySelector("#container");
images.forEach(function(image) {
  image.active = false;
  image.currentX;
  image.currentY;
  image.initialX;
  image.initialY;
  image.xOffset = 0;
  image.yOffset = 0;
})

container.addEventListener("touchstart", dragStart, false);
container.addEventListener("touchend", dragEnd, false);
container.addEventListener("touchmove", draggable, false);

container.addEventListener("mousedown", dragStart, false);
container.addEventListener("mouseup", dragEnd, false);
container.addEventListener("mousemove", draggable, false);

function dragStart(e) {
  images.forEach(function(image) {
    image.initialX = e.clientX - image.xOffset;
    image.initialY = e.clientY - image.yOffset;
    if (e.target === image) {
      image.active = true;
    }
  })
}

function dragEnd(e) {
  images.forEach(function(image) {
    image.initialX = image.currentX;
    image.initialY = image.currentY;
    image.active = false;
  })
}

function draggable(e) {
  images.forEach(function(image) {
    if (image.active) {
      e.preventDefault();

      image.currentX = e.clientX - image.initialX;
      image.currentY = e.clientY - image.initialY;
      image.xOffset = image.currentX;
      image.yOffset = image.currentY;

      imageTranslate(image.currentX, image.currentY, image);
    }
  })
}

function imageTranslate(xPos, yPos, element) {
  element.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
}



function permissionDrop(e) {
  e.preventDefault();
}

function drag(e) {
  e.dataTransfer.setData("text", e.target.id);
}

function drop(e) {
  e.preventDefault();
  var data = e.dataTransfer.getData("text");
  e.target.appendChild(document.getElementById(data));
}